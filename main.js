// GLOBAL VARIABLES

const arcadeMonthlyPrice = 9
const arcadeYearlyPrice = 90
const advancedMonthlyPrice = 12
const advancedYearlyPrice = 120
const proMonthlyPrice = 15
const proYearlyPrice = 150

const onlineServiceMonthlyPrice = 1
const onlineServiceYearlyPrice = 10
const largerStorageMonthlyPrice = 2
const largerStorageYearlyPrice = 20
const customizableProfileMonthlyPrice = 2
const customizableProfileYearlyPrice = 20

// MAIN CONTAINER ELEMENTS

let step1Container = document.getElementsByClassName("your-info-container")
let step2Container = document.getElementsByClassName(
    "select-your-plan-container"
)
let step3Container = document.getElementsByClassName("pick-add-ons-container")
let step4Container = document.getElementsByClassName("summary-container")
let thankYouContainer = document.getElementsByClassName("thank-you-container")

// NAVIGATION ELEMENTS

let step1NextStep = document.getElementsByClassName("step-1-next-step")
let step2GoBack = document.getElementsByClassName("step-2-go-back")
let step2NextStep = document.getElementsByClassName("step-2-next-step")
let step3GoBack = document.getElementsByClassName("step-3-go-back")
let step3NextStep = document.getElementsByClassName("step-3-next-step")
let step4GoBack = document.getElementsByClassName("step-4-go-back")
let step4NextStep = document.getElementsByClassName("step-4-next-step")

let step1NextStepMobile = document.getElementsByClassName(
    "step-1-next-step-mobile"
)
let step2GoBackMobile = document.getElementsByClassName("step-2-go-back-mobile")
let step2NextStepMobile = document.getElementsByClassName(
    "step-2-next-step-mobile"
)
let step3GoBackMobile = document.getElementsByClassName("step-3-go-back-mobile")
let step3NextStepMobile = document.getElementsByClassName(
    "step-3-next-step-mobile"
)
let step4GoBackMobile = document.getElementsByClassName("step-4-go-back-mobile")
let step4NextStepMobile = document.getElementsByClassName(
    "step-4-next-step-mobile"
)

let step1BottomNavMobile = document.getElementsByClassName(
    "step-1-bottom-nav-mobile"
)
let step2BottomNavMobile = document.getElementsByClassName(
    "step-2-bottom-nav-mobile"
)
let step3BottomNavMobile = document.getElementsByClassName(
    "step-3-bottom-nav-mobile"
)
let step4BottomNavMobile = document.getElementsByClassName(
    "step-4-bottom-nav-mobile"
)

// STEPS CONTAINER ELEMENTS

let step1Number = document.getElementsByClassName("step-1-number")
let step2Number = document.getElementsByClassName("step-2-number")
let step3Number = document.getElementsByClassName("step-3-number")
let step4Number = document.getElementsByClassName("step-4-number")

// STEP 1 FORM ELEMENTS

let userName = document.getElementById("name")
let userNameLabelField = document.getElementsByClassName("label-field-1")
let emailAddress = document.getElementById("email")
let emailAddressLabelField = document.getElementsByClassName("label-field-2")
let phoneNo = document.getElementById("phoneNo")
let phoneNoLabelField = document.getElementsByClassName("label-field-3")

// STEP 2 ELEMENTS

let arcadeMonthlyPriceField = document.getElementsByClassName(
    "arcade-monthly-price-field"
)
arcadeMonthlyPriceField[0].textContent = arcadeMonthlyPrice
let arcadeYearlyPriceField = document.getElementsByClassName(
    "arcade-yearly-price-field"
)
arcadeYearlyPriceField[0].textContent = arcadeYearlyPrice
let advancedMonthlyPriceField = document.getElementsByClassName(
    "advanced-monthly-price-field"
)
advancedMonthlyPriceField[0].textContent = advancedMonthlyPrice
let advancedYearlyPriceField = document.getElementsByClassName(
    "advanced-yearly-price-field"
)
advancedYearlyPriceField[0].textContent = advancedYearlyPrice
let proMonthlyPriceField = document.getElementsByClassName(
    "pro-monthly-price-field"
)
proMonthlyPriceField[0].textContent = proMonthlyPrice
let proYearlyPriceField = document.getElementsByClassName(
    "pro-yearly-price-field"
)
proYearlyPriceField[0].textContent = proYearlyPrice
let yearlyDiscountField = document.getElementsByClassName("yearly-discount")

let togglePlan = document.getElementById("change-plan")

let arcadePriceMonthly = document.getElementsByClassName("arcade-price-monthly")
let arcadePriceYearly = document.getElementsByClassName("arcade-price-yearly")
let advancedPriceMonthly = document.getElementsByClassName(
    "advanced-price-monthly"
)
let advacnedPriceYearly = document.getElementsByClassName(
    "advanced-price-yearly"
)
let proPriceMonthly = document.getElementsByClassName("pro-price-monthly")
let proPriceYearly = document.getElementsByClassName("pro-price-yearly")

let arcadePlanCard = document.getElementsByClassName("arcade-plan")
let advancedPlanCard = document.getElementsByClassName("advanced-plan")
let proPlanCard = document.getElementsByClassName("pro-plan")

let noPlanSelectedField = document.getElementsByClassName("no-plan-selected")

let monthlyBilling = document.getElementsByClassName("monthly-billing")
let yearlyBilling = document.getElementsByClassName("yearly-billing")

// STEP 3 ELEMENTS

let onlineServiceContainer = document.getElementsByClassName("online-service")
let largerStorageContainer = document.getElementsByClassName("larger-storage")
let customizableProfileContainer = document.getElementsByClassName(
    "customizable-profile"
)

let onlineServiceCheck = document.getElementById("online-service-check")
let largerStorageCheck = document.getElementById("larger-storage-check")
let customizableProfileCheck = document.getElementById(
    "customizable-profile-check"
)

let onlineServiceMonthlyPriceField = document.getElementsByClassName(
    "monthly-online-service-price-field"
)
let onlineServiceYearlyPriceField = document.getElementsByClassName(
    "yearly-online-service-price-field"
)
let largerStorageMonthlyPriceField = document.getElementsByClassName(
    "monthly-larger-storage-price-field"
)
let largerStorageYearlyPriceField = document.getElementsByClassName(
    "yearly-larger-storage-price-field"
)
let customizableProfileMonthlyPriceField = document.getElementsByClassName(
    "monthly-customizable-profile-price-field"
)
let customizableProfileYearlyPriceField = document.getElementsByClassName(
    "yearly-customizable-profile-price-field"
)

let monthlyOnlineServicePrice = document.getElementsByClassName(
    "monthly-online-service-price"
)
monthlyOnlineServicePrice[0].textContent = onlineServiceMonthlyPrice
let yearlyOnlineServicePrice = document.getElementsByClassName(
    "yearly-online-service-price"
)
yearlyOnlineServicePrice[0].textContent = onlineServiceYearlyPrice
let monthlyLargerStoragePrice = document.getElementsByClassName(
    "monthly-larger-storage-price"
)
monthlyLargerStoragePrice[0].textContent = largerStorageMonthlyPrice
let yearlyLargerStoragePrice = document.getElementsByClassName(
    "yearly-larger-storage-price"
)
yearlyLargerStoragePrice[0].textContent = largerStorageYearlyPrice
let monthlyCustomizableProfilePrice = document.getElementsByClassName(
    "monthly-customizable-profile-price"
)
monthlyCustomizableProfilePrice[0].textContent = customizableProfileMonthlyPrice
let yearlyCustomizableProfilePrice = document.getElementsByClassName(
    "yearly-customizable-profile-price"
)
yearlyCustomizableProfilePrice[0].textContent = customizableProfileYearlyPrice

// SUMMARY ELEMENTS

let planCardSummary = document.getElementsByClassName("plan-card-summary")
let planTypeSummary = document.getElementsByClassName("plan-type-summary")
let planPriceAmount = document.getElementsByClassName("plan-price-amount")
let perPlanType = document.getElementsByClassName("per-plan-type")
let onlineServicePriceAmountSummary = document.getElementsByClassName(
    "online-service-price-amount-summary"
)
let largerStoragePriceAmountSummary = document.getElementsByClassName(
    "larger-storage-price-amount-summary"
)
let customizableProfilePriceAmountSummary = document.getElementsByClassName(
    "customizable-profile-price-amount-summary"
)
let totalPerPlanType = document.getElementsByClassName("total-per-plan-type")
let totalPriceAmount = document.getElementsByClassName("total-price-amount")

let onlineServiceSummary = document.getElementsByClassName(
    "online-service-summary"
)
let largerStorageSummary = document.getElementsByClassName(
    "larger-storage-summary"
)
let customizableProfileSummary = document.getElementsByClassName(
    "customizable-profile-summary"
)

let changePlanSummary = document.getElementsByClassName("change-plan-summary")

// DOM CONTENT LOAD

document.addEventListener("DOMContentLoaded", () => {
    step1Container[0].style.display = "flex"
    step2Container[0].style.display = "none"
    step3Container[0].style.display = "none"
    step4Container[0].style.display = "none"

    step1BottomNavMobile[0].style.display = "flex"
    step2BottomNavMobile[0].style.display = "none"
    step3BottomNavMobile[0].style.display = "none"
    step4BottomNavMobile[0].style.display = "none"
    

    localStorage.removeItem("planSelected")
    localStorage.removeItem("onlineServiceAddOn")
    localStorage.removeItem("largerStorageAddOn")
    localStorage.removeItem("customizableProfileAddOn")
    localStorage.setItem("planType", "monthly")

    step1Number[0].style.backgroundColor = "#bce1fd"
    adjustPrices()
})

// STEPS NAVIGATION DESKTOP

step1NextStep[0].addEventListener("click", () => {
    if (checkStep1Validation()) {
        let userDetails = {
            email: emailAddress.value,
            name: userName.value,
            phone: phoneNo.value,
        }
        localStorage.setItem(
            emailAddress.value,
            JSON.stringify(userDetails, null, 2)
        )
        step1Container[0].style.display = "none"
        step2Container[0].style.display = "flex"

        step1Number[0].style.backgroundColor = "#483eff"
        step2Number[0].style.backgroundColor = "#bce1fd"
    }
})

step2GoBack[0].addEventListener("click", () => {
    step2Container[0].style.display = "none"
    step1Container[0].style.display = "flex"
})

step2NextStep[0].addEventListener("click", () => {
    if (anyPlanSelected()) {
        adjustPrices()
        step2Container[0].style.display = "none"
        step3Container[0].style.display = "flex"

        step2Number[0].style.backgroundColor = "#483eff"
        step3Number[0].style.backgroundColor = "#bce1fd"
    }
})

step3GoBack[0].addEventListener("click", () => {
    step3Container[0].style.display = "none"
    step2Container[0].style.display = "flex"
})

step3NextStep[0].addEventListener("click", () => {
    createPricingSummary()
    step3Container[0].style.display = "none"
    step4Container[0].style.display = "flex"

    step3Number[0].style.backgroundColor = "#483eff"
    step4Number[0].style.backgroundColor = "#bce1fd"
})

step4GoBack[0].addEventListener("click", () => {
    step4Container[0].style.display = "none"
    step3Container[0].style.display = "flex"
})

step4NextStep[0].addEventListener("click", () => {
    step4Container[0].style.display = "none"
    thankYouContainer[0].style.display = "flex"
})

/* STEPS NAVIGATION MOBILE */

step1NextStepMobile[0].addEventListener("click", () => {
    if (checkStep1Validation()) {
        let userDetails = {
            email: emailAddress.value,
            name: userName.value,
            phone: phoneNo.value,
        }
        localStorage.setItem(
            emailAddress.value,
            JSON.stringify(userDetails, null, 2)
        )
        step1Container[0].style.display = "none"
        step2Container[0].style.display = "flex"

        step1Number[0].style.backgroundColor = "#483eff"
        step2Number[0].style.backgroundColor = "#bce1fd"

        step1BottomNavMobile[0].style.display = "none"
        step2BottomNavMobile[0].style.display = "flex"
    }
})

step2GoBackMobile[0].addEventListener("click", () => {
    step2Container[0].style.display = "none"
    step1Container[0].style.display = "flex"

    step2BottomNavMobile[0].style.display = "none"
    step1BottomNavMobile[0].style.display = "flex"
})

step2NextStepMobile[0].addEventListener("click", () => {
    if (anyPlanSelected()) {
        adjustPrices()
        step2Container[0].style.display = "none"
        step3Container[0].style.display = "flex"

        step2Number[0].style.backgroundColor = "#483eff"
        step3Number[0].style.backgroundColor = "#bce1fd"

        step2BottomNavMobile[0].style.display = "none"
        step3BottomNavMobile[0].style.display = "flex"
    }
})

step3GoBackMobile[0].addEventListener("click", () => {
    step3Container[0].style.display = "none"
    step2Container[0].style.display = "flex"

    step3BottomNavMobile[0].style.display = "none"
    step2BottomNavMobile[0].style.display = "flex"
})

step3NextStepMobile[0].addEventListener("click", () => {
    createPricingSummary()
    step3Container[0].style.display = "none"
    step4Container[0].style.display = "flex"

    step3Number[0].style.backgroundColor = "#483eff"
    step4Number[0].style.backgroundColor = "#bce1fd"

    step3BottomNavMobile[0].style.display = "none"
    step4BottomNavMobile[0].style.display = "flex"
})

step4GoBackMobile[0].addEventListener("click", () => {
    step4Container[0].style.display = "none"
    step3Container[0].style.display = "flex"

    step4BottomNavMobile[0].style.display = "none"
    step3BottomNavMobile[0].style.display = "flex"
})

step4NextStepMobile[0].addEventListener("click", () => {
    step4Container[0].style.display = "none"
    thankYouContainer[0].style.display = "flex"

    step4BottomNavMobile[0].style.display = "none"
})

// FORM VALIDATION

const checkStep1Validation = () => {
    let check1 = checkUserName(userName)
    let check2 = checkEmailAddress(emailAddress)
    let check3 = checkPhoneNo(phoneNo)
    if (check1 === true && check2 === true && check3 === true) {
        return true
    } else {
        return false
    }
}

const checkUserName = (userName) => {
    if (userName.value === "") {
        userNameLabelField[0].children[1].style.display = "flex"
        userNameLabelField[0].children[2].style.display = "none"
        userName.style.border = "solid 1px red"
        return false
    } else {
        userNameLabelField[0].children[1].style.display = "none"
        if (validateUserName(userName)) {
            userName.style.border = "solid 1px #cdccd1"
            return true
        } else {
            userName.style.border = "solid 1px red"
            return false
        }
    }
}

const validateUserName = (userName) => {
    var letters = /^[A-Za-z\s]+$/
    if (userName.value.match(letters)) {
        userNameLabelField[0].children[2].style.display = "none"
        return true
    } else {
        userNameLabelField[0].children[2].style.display = "flex"
        return false
    }
}

const checkEmailAddress = (emailAddress) => {
    if (emailAddress.value === "") {
        emailAddressLabelField[0].children[1].style.display = "flex"
        emailAddressLabelField[0].children[2].style.display = "none"
        emailAddress.style.border = "solid 1px red"
        return false
    } else {
        emailAddressLabelField[0].children[1].style.display = "none"
        if (validateEmail(emailAddress.value)) {
            emailAddress.style.border = "solid 1px #cdccd1"
            return true
        } else {
            emailAddress.style.border = "solid 1px red"
            return false
        }
    }
}

const validateEmail = (emailAddress) => {
    let atpos = emailAddress.indexOf("@")
    let dotpos = emailAddress.lastIndexOf(".")
    if (atpos < 1 || dotpos - atpos < 2) {
        emailAddressLabelField[0].children[2].style.display = "flex"
        return false
    } else {
        emailAddressLabelField[0].children[2].style.display = "none"
        return true
    }
}

const checkPhoneNo = (phoneNo) => {
    if (phoneNo.value === "") {
        phoneNoLabelField[0].children[1].style.display = "flex"
        phoneNoLabelField[0].children[2].style.display = "none"
        phoneNo.style.border = "solid 1px red"
        return false
    } else {
        phoneNoLabelField[0].children[1].style.display = "none"
        if (validatePhoneNo(phoneNo)) {
            phoneNo.style.border = "solid 1px #cdccd1"
            return true
        } else {
            phoneNo.style.border = "solid 1px red"
            return false
        }
    }
}

const validatePhoneNo = (phoneNo) => {
    var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{10})$/
    if (phoneNo.value.match(phoneno)) {
        phoneNoLabelField[0].children[2].style.display = "none"
        return true
    } else {
        phoneNoLabelField[0].children[2].style.display = "flex"
        return false
    }
}

// SELECT YOUR PLAN

const changePlan = () => {
    if (togglePlan.checked == true) {
        arcadePriceYearly[0].style.display = "flex"
        advacnedPriceYearly[0].style.display = "flex"
        proPriceYearly[0].style.display = "flex"
        for (let index = 0; index < yearlyDiscountField.length; index++) {
            yearlyDiscountField[index].style.display = "flex"
        }
        arcadePriceMonthly[0].style.display = "none"
        advancedPriceMonthly[0].style.display = "none"
        proPriceMonthly[0].style.display = "none"
        localStorage.setItem("planType", "yearly")
        yearlyBilling[0].style.color = "hsl(213, 96%, 18%)"
        monthlyBilling[0].style.color = "hsl(231, 11%, 63%)"
    } else {
        arcadePriceYearly[0].style.display = "none"
        advacnedPriceYearly[0].style.display = "none"
        proPriceYearly[0].style.display = "none"
        for (let index = 0; index < yearlyDiscountField.length; index++) {
            yearlyDiscountField[index].style.display = "none"
        }
        arcadePriceMonthly[0].style.display = "flex"
        advancedPriceMonthly[0].style.display = "flex"
        proPriceMonthly[0].style.display = "flex"
        localStorage.setItem("planType", "monthly")
        monthlyBilling[0].style.color = "hsl(213, 96%, 18%)"
        yearlyBilling[0].style.color = "hsl(231, 11%, 63%)"
    }
}

arcadePlanCard[0].addEventListener("click", () => {
    noPlanSelectedField[0].style.display = "none"
    arcadePlanCard[0].style.backgroundColor = "#D7D8DA"
    advancedPlanCard[0].style.backgroundColor = "#ffffff"
    proPlanCard[0].style.backgroundColor = "#ffffff"
    localStorage.setItem("planSelected", "arcade")
})

advancedPlanCard[0].addEventListener("click", () => {
    noPlanSelectedField[0].style.display = "none"
    arcadePlanCard[0].style.backgroundColor = "#ffffff"
    advancedPlanCard[0].style.backgroundColor = "#D7D8DA"
    proPlanCard[0].style.backgroundColor = "#ffffff"
    localStorage.setItem("planSelected", "advanced")
})

proPlanCard[0].addEventListener("click", () => {
    noPlanSelectedField[0].style.display = "none"
    arcadePlanCard[0].style.backgroundColor = "#ffffff"
    advancedPlanCard[0].style.backgroundColor = "#ffffff"
    proPlanCard[0].style.backgroundColor = "#D7D8DA"
    localStorage.setItem("planSelected", "pro")
})

const anyPlanSelected = () => {
    let plan = localStorage.getItem("planSelected")
    if (plan == "arcade" || plan == "advanced" || plan == "pro") {
        return true
    } else {
        noPlanSelectedField[0].style.display = "flex"
        return false
    }
}

// PICK ADD ONS

const adjustPrices = () => {
    let planType = localStorage.getItem("planType")
    if (planType == "monthly") {
        onlineServiceMonthlyPriceField[0].style.display = "flex"
        largerStorageMonthlyPriceField[0].style.display = "flex"
        customizableProfileMonthlyPriceField[0].style.display = "flex"
        onlineServiceYearlyPriceField[0].style.display = "none"
        largerStorageYearlyPriceField[0].style.display = "none"
        customizableProfileYearlyPriceField[0].style.display = "none"
        monthlyBilling[0].style.color = "hsl(213, 96%, 18%)"
        yearlyBilling[0].style.color = "hsl(231, 11%, 63%)"
    } else {
        onlineServiceMonthlyPriceField[0].style.display = "none"
        largerStorageMonthlyPriceField[0].style.display = "none"
        customizableProfileMonthlyPriceField[0].style.display = "none"
        onlineServiceYearlyPriceField[0].style.display = "flex"
        largerStorageYearlyPriceField[0].style.display = "flex"
        customizableProfileYearlyPriceField[0].style.display = "flex"
        monthlyBilling[0].style.color = "hsl(231, 11%, 63%)"
        yearlyBilling[0].style.color = "hsl(213, 96%, 18%)"
    }
}

onlineServiceCheck.addEventListener("click", () => {
    if (onlineServiceCheck.checked == true) {
        onlineServiceContainer[0].style.backgroundColor = "#f8f9fe"
        localStorage.setItem("onlineServiceAddOn", true)
    } else {
        onlineServiceContainer[0].style.backgroundColor = "#ffffff"
        localStorage.setItem("onlineServiceAddOn", false)
    }
})

largerStorageCheck.addEventListener("click", () => {
    if (largerStorageCheck.checked == true) {
        largerStorageContainer[0].style.backgroundColor = "#f8f9fe"
        localStorage.setItem("largerStorageAddOn", true)
    } else {
        largerStorageContainer[0].style.backgroundColor = "#ffffff"
        localStorage.setItem("largerStorageAddOn", false)
    }
})

customizableProfileCheck.addEventListener("click", () => {
    if (customizableProfileCheck.checked == true) {
        customizableProfileContainer[0].style.backgroundColor = "#f8f9fe"
        localStorage.setItem("customizableProfileAddOn", true)
    } else {
        customizableProfileContainer[0].style.backgroundColor = "#ffffff"
        localStorage.setItem("customizableProfileAddOn", false)
    }
})

// FINISHING UP

const createPricingSummary = () => {
    let planType = localStorage.getItem("planType")
    let planSelected = localStorage.getItem("planSelected")
    let totalPrice = 0
    let onlineServiceAddOn = localStorage.getItem("onlineServiceAddOn")
    let largerStorageAddOn = localStorage.getItem("largerStorageAddOn")
    let customizableProfileAddOn = localStorage.getItem(
        "customizableProfileAddOn"
    )

    if (planType == "monthly") {
        planTypeSummary[0].textContent = "Monthly"
        for (let index = 0; index < perPlanType.length; index++) {
            perPlanType[index].textContent = "mo"
        }
        totalPerPlanType[0].textContent = " month"
        onlineServicePriceAmountSummary[0].textContent =
            onlineServiceMonthlyPrice
        largerStoragePriceAmountSummary[0].textContent =
            largerStorageMonthlyPrice
        customizableProfilePriceAmountSummary[0].textContent =
            customizableProfileMonthlyPrice
    } else if (planType == "yearly") {
        planTypeSummary[0].textContent = "Yearly"
        for (let index = 0; index < perPlanType.length; index++) {
            perPlanType[index].textContent = "yr"
        }
        totalPerPlanType[0].textContent = "year"
        onlineServicePriceAmountSummary[0].textContent =
            onlineServiceYearlyPrice
        largerStoragePriceAmountSummary[0].textContent =
            largerStorageYearlyPrice
        customizableProfilePriceAmountSummary[0].textContent =
            customizableProfileYearlyPrice
    }

    if (planSelected == "arcade") {
        planCardSummary[0].textContent = "Arcade"
        if (planType == "monthly") {
            planPriceAmount[0].textContent = arcadeMonthlyPrice
            totalPrice = arcadeMonthlyPrice
        } else if (planType == "yearly") {
            planPriceAmount[0].textContent = arcadeYearlyPrice
            totalPrice = arcadeYearlyPrice
        }
    } else if (planSelected == "advanced") {
        planCardSummary[0].textContent = "Advanced"
        if (planType == "monthly") {
            planPriceAmount[0].textContent = advancedMonthlyPrice
            totalPrice = advancedMonthlyPrice
        } else if (planType == "yearly") {
            planPriceAmount[0].textContent = advancedYearlyPrice
            totalPrice = advancedYearlyPrice
        }
    } else if (planSelected == "pro") {
        planCardSummary[0].textContent = "Pro"
        if (planType == "monthly") {
            planPriceAmount[0].textContent = proMonthlyPrice
            totalPrice = proMonthlyPrice
        } else if (planType == "yearly") {
            planPriceAmount[0].textContent = proYearlyPrice
            totalPrice = proYearlyPrice
        }
    }

    if (onlineServiceAddOn == "true") {
        onlineServiceSummary[0].style.display = "flex"
        if (planType == "monthly") {
            totalPrice = totalPrice + onlineServiceMonthlyPrice
        } else if (planType == "yearly") {
            totalPrice = totalPrice + onlineServiceYearlyPrice
        }
    } else {
        onlineServiceSummary[0].style.display = "none"
    }

    if (largerStorageAddOn == "true") {
        largerStorageSummary[0].style.display = "flex"
        if (planType == "monthly") {
            totalPrice = totalPrice + largerStorageMonthlyPrice
        } else if (planType == "yearly") {
            totalPrice = totalPrice + largerStorageYearlyPrice
        }
    } else {
        largerStorageSummary[0].style.display = "none"
    }

    if (customizableProfileAddOn == "true") {
        customizableProfileSummary[0].style.display = "flex"
        if (planType == "monthly") {
            totalPrice = totalPrice + customizableProfileMonthlyPrice
        } else if (planType == "yearly") {
            totalPrice = totalPrice + customizableProfileYearlyPrice
        }
    } else {
        customizableProfileSummary[0].style.display = "none"
    }

    totalPriceAmount[0].textContent = totalPrice
}

changePlanSummary[0].addEventListener("click", () => {
    step4Container[0].style.display = "none"
    step2Container[0].style.display = "flex"

    step4Number[0].style.backgroundColor = "#483eff"
    step2Number[0].style.backgroundColor = "#bce1fd"

    step4BottomNavMobile[0].style.display = "none"
    step2BottomNavMobile[0].style.display = "flex"
})
